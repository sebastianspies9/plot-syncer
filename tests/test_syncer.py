from plot_syncer import syncer
from plot_syncer.window import Window


def test_should_start():
    windows = []
    assert not syncer.resolve_window(windows, 0)

    windows = [Window(0, 24, 50)]
    assert syncer.resolve_window(windows, 0) == windows[0]
    assert syncer.resolve_window(windows, 23) == windows[0]

    assert syncer.resolve_window([Window(0, 23, 0)], 0)
    assert syncer.resolve_window([Window(0, 23, 0)], 1)
    assert not syncer.resolve_window([Window(0, 23, 0)], 23)
    assert not syncer.resolve_window([Window(0, 0, 0)], 23)
    assert syncer.resolve_window([Window(0, 23, 0)], 1)
    assert syncer.resolve_window([Window(0, 23, 0)], 22)
    assert not syncer.resolve_window([Window(0, 23, 0)], 23)
    # wrap around
    assert not syncer.resolve_window([Window(1, 0, 0)], 0)
    assert syncer.resolve_window([Window(1, 0, 0)], 1)
    assert syncer.resolve_window([Window(1, 0, 0)], 23)
    assert syncer.resolve_window([Window(1, 0, 0)], 1)

    assert syncer.resolve_window([Window(22, 4, 0)], 22)
    assert syncer.resolve_window([Window(22, 4, 0)], 23)
    assert syncer.resolve_window([Window(22, 4, 0)], 0)
    assert syncer.resolve_window([Window(22, 4, 0)], 3)
    assert not syncer.resolve_window([Window(22, 4, 0)], 4)
