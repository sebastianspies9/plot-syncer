import logging
import sys
import argparse
import yaml
from plot_syncer.window import Window


def main():
    from plot_syncer import syncer

    parser = argparse.ArgumentParser(
        description="Synchronizing using rsync at specific hours"
    )
    parser.add_argument("source", type=str, help="source rsync compatible URL")
    parser.add_argument("destination", type=str, help="local directory")
    parser.add_argument(
        "--config",
        type=str,
        help="configuration_file",
        default="./config.yaml",
    )
    parser.add_argument(
        "-v",
        dest="verbose",
        help="verbose",
        action="store_true",
    )

    args = parser.parse_args()
    logging.basicConfig(format="%(asctime)-15s %(message)s")
    root = logging.getLogger()
    if args.verbose:
        root.setLevel(logging.INFO)
    else:
        root.setLevel(logging.WARNING)

    windows = load_windows(args.config)
    syncer.synchronize(args.source, args.destination, windows, args.verbose)


def load_windows(filename):
    with open(filename) as file:
        yaml_dict = yaml.full_load(file.read())
        return [
            Window.from_dict(window_dict)
            for window_dict in yaml_dict["synchronization-windows"]
        ]


if __name__ == "__main__":
    import sys

    sys.path.append("./src")
    main()
