# Plot Syncer

## Purpose

Synchronize files one-way from an ssh server to a client only during defined hours.

## Requirements

- docker
- docker-compose

## Configure

Edit docker-compose.yaml

```yaml
version: '3'

services:
  plot-syncer:
    build: .
    volumes:
      - $HOME/.ssh:/root/.ssh-import:ro
      - $PWD/test:/test
    command:
      user@host:test/ /test 22 9 -v
```

Change your directories accordingly. In this case, the /test directory inside the container is mounted to $PWD/test.

The rsync process then synchronizes to /test inside the container (second argument of command)

Define hours. In this example, the rsync is repeated between 23 hour and 6 hour local time.

Make sure, you have generated `id_rsa.pub`, `id_rsa` and rolled it out to `user@host:.ssh/authorized_keys`
using `ssh-keygen -t rsa` and `ssh-copy-id user@host`.

Please note: The last sync may be started before 6am, but may not terminate before 6am.

## Usage

### Start service

```bash
$ docker-compose up -d
Creating network "plot-syncer_default" with the default driver
Building plot-syncer
Step 1/8 : FROM python:3.8
 ---> 53da5c105f01
Step 2/8 : RUN apt-get update; apt-get install -y rsync; apt-get clean
 ---> Using cache
 ---> 1aecc93ef725
Step 3/8 : WORKDIR /app
 ---> Using cache
 ---> c795ce47636f
Step 4/8 : COPY ./requirements.txt /app
 ---> e2f6870070cc
Step 5/8 : RUN pip install -r requirements.txt
 ---> Running in 44588d4cc566
Collecting setuptools~=54.2.0
  Downloading setuptools-54.2.0-py3-none-any.whl (785 kB)
Installing collected packages: setuptools
  Attempting uninstall: setuptools
    Found existing installation: setuptools 54.1.1
    Uninstalling setuptools-54.1.1:
      Successfully uninstalled setuptools-54.1.1
Successfully installed setuptools-54.2.0
Removing intermediate container 44588d4cc566
 ---> 0c558955f00f
Step 6/8 : COPY ./src /app
 ---> b1c2bca9cd11
Step 7/8 : COPY ./main.py /app
 ---> 94926f489a93
Step 8/8 : ENTRYPOINT [ "python", "main.py" ]
 ---> Running in 6261a4f9ed20
Removing intermediate container 6261a4f9ed20
 ---> 1bdb4bd28789
Successfully built 1bdb4bd28789
Successfully tagged plot-syncer_plot-syncer:latest
WARNING: Image for service plot-syncer was built because it did not already exist. To rebuild this image you must use `docker-compose build` or `docker-compose up --build`.
Creating plot-syncer_plot-syncer_1 ... done
```

### Show running services

```bash
$ docker-compose ps
          Name                         Command               State   Ports
--------------------------------------------------------------------------
plot-syncer_plot-syncer_1   python main.py user@jupi ...   Up
```

### Show log output

```bash          
$ docker-compose logs -f
Attaching to plot-syncer_plot-syncer_1
plot-syncer_1  | Warning: Permanently added 'host' (ECDSA) to the list of known hosts.
plot-syncer_1  | receiving incremental file list
plot-syncer_1  | 
plot-syncer_1  | sent 20 bytes  received 82 bytes  22.67 bytes/sec
plot-syncer_1  | total size is 0  speedup is 0.00
plot-syncer_1  | receiving incremental file list
plot-syncer_1  | 
plot-syncer_1  | sent 20 bytes  received 82 bytes  22.67 bytes/sec
plot-syncer_1  | total size is 0  speedup is 0.00
plot-syncer_1  | receiving incremental file list
plot-syncer_1  | 
plot-syncer_1  | sent 20 bytes  received 82 bytes  22.67 bytes/sec
plot-syncer_1  | total size is 0  speedup is 0.00
plot-syncer_1  | receiving incremental file list
plot-syncer_1  | 
plot-syncer_1  | sent 20 bytes  received 82 bytes  22.67 bytes/sec
plot-syncer_1  | total size is 0  speedup is 0.00
plot-syncer_1  | receiving incremental file list
plot-syncer_1  | 
plot-syncer_1  | sent 20 bytes  received 82 bytes  22.67 bytes/sec
plot-syncer_1  | total size is 0  speedup is 0.00
plot-syncer_1  | receiving incremental file list
plot-syncer_1  | deleting bla.txt
plot-syncer_1  | ./

```

### Help

```bash
$ docker-compose run --rm plot-syncer -h
usage: main.py [-h] [-v] source destination from_hour to_hour

Synchronizing using rsync at specific hours

positional arguments:
  source       source rsync compatible URL
  destination  local directory
  from_hour    starting hour of synchronization window
  to_hour      ending hour of synchronization window

optional arguments:
  -h, --help   show this help message and exit
  -v           verbose
```
