from dataclasses import dataclass


@dataclass
class Window:
    from_hour: int
    to_hour: int
    rate_limit: int

    @staticmethod
    def from_dict(value):
        return Window(
            from_hour=value["from"],
            to_hour=value["to"],
            rate_limit=value["rate-limit"],
        )
